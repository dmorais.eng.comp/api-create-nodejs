const express = require("express");
const server = express();


server.use(express.json());

// GET
server.get("/", (request, response) => {
    response.send("<p>Hello World</p>");

});


// GET /users
server.get("/users", (request, response) => {
    const users = [];
    response.send({users});
});


// POST /users
// 2. Modifier status
server.post("/users", (request, response) => {
    console.log(request.body.user);

    // Vérifications
    if (request.body.email) {

    } else {

    }

// Logique
    response.statusCode = 404;
    response.send({message: "User saved", user: "Sam", req: request.body.user});
});

// 4. PUT /users/:id
server.put("/users/:id", (request, response) => {
    response.send({message: "User updated successfully", user: request.params.id, data: request.body});
});

// DELETE /users/:id

// 1. Supprimer user et modifier status
server.delete("/users/:id", (request, response) => {
    console.log(request.params);

    // Vérifie que l'id correspond à un utilisateur dans la base de données

    const userExist = false;
    if (userExist === false) {
        response.status(404).send('User not found : ' + request.params.id);
    }

    response.status(201).send({message: "User successfully deleted", user: request.params.id});
});


server.listen(8080, () => {
    console.log("Server running on http://localhost:8080");
});