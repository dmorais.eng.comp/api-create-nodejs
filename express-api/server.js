const express = require("express");
const cors = require("cors");

const {Sequelize, DataTypes} = require("sequelize");

// Créer la connexion à MySQL
const sequelize = new Sequelize("app", "root", "****", {
    host: "127.0.0.1",
    dialect: "mysql"

}); // ("base de donnée", "utilisateur", "mdp", {options})

// Tester la connection à MySQL
sequelize.authenticate().then(() => {
    console.log("Connection successfull");
}).catch((error) => {
    console.error("Unable to connect to the database", error);
});

// Déclarer le modèle User

const User = sequelize.define("User",
    {
        firstname: {
            type: DataTypes.STRING,
            allowNull: false
        },
        lastname: {
            type: DataTypes.STRING,
            allowNull: false
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        note: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        // // Rajouter une nouvelle colonne depuis l'api: FONCTIONNE AVEC L'OPTION ALTER
        // presence: {
        //     type: DataTypes.STRING,
        //     allowNull: true
    }, {
        sequelize,
        tableName: "users",
        createdAt: false, // pour désactiver car s'ajoute automatiquement
        updatedAt: false, // désactiver
    });

// Déclarer le modèle Post, columns: title, text

const Post = sequelize.define("Post", {
    title: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    text: {
        type: DataTypes.TEXT,
        allowNull: false,
    }
}, {
    sequelize,
    tableName: "posts",
    createdAt: false,
    updatedAt: false,
});

// Définir la relation: User has many Post (peut avoir plusieurs)

User.hasMany(Post, {
    as: "posts",
    foreignKey: "fk_author_id",
    sourceKey: "id",
    constraints: true,
    onDelete: "cascade",
});

// Synchroniser le model User
//
// User.sync({
//     alter: true // Vérifie l'état actuel de la table
// })
//     .then(() => {
//         console.log("Sync User success");
//     })
//     .catch((error) => {
//     console.log("Sync User error :", error);
// });

// Synchroniser tout les models

sequelize.sync({
    alter: true
})
    .then(() => {
        console.log("Sync User success");
    })
    .catch((error) => {
        console.log("Sync User error :", error);
    });


const server = express();
const PORT = 8080;

server.use(cors({
    origin: "*",
    optionsSuccessStatus: 200
}));

server.use(express.json());


/* - GET /users : Récupère la liste de tous les utilisateurs
Aucune donnée envoyé avec la requête
Retourne un tableau d'utilisateurs
 */

server.get("/users", (request, response) => {
    // Recherche
    User.findAll()
    // Récupérer les Posts des utilisateurs
    include: [{model: Post, as: "posts"}]
        // Réaction
        .then((users) => {
            // attributes: ["id", "firstname"] // Selectionner attributs
            // limit: 2 // Va limiter à 2 utilisateurs
            response.send(users);
        }).catch((err) => {
            response.statusCode = 404;
            response.send("List not found : ", + err);
        });

});

/*
- POST /users : Enregistre un utilisateur avec les données que je t'envoie
Envoyez dans la requête une donnée au format JSON : firstName, lastName, email
Retourne un objet contenant les informations que vous lui avez envoyé et un message disant "Utilisateur enregistré !"
*/

server.post("/users", (request, response) => {
    console.log(request.body);

    User.create({
        firstname: request.body.firstName,
        lastname: request.body.lastName,
        email: request.body.email
    }).then((user) => {
        response.send({message: "Users has been saved!", user});
    }).catch((err) => {
        response.statusCode = 404;
        response.send({message: "User cannot be saved", err});
    });

});


/*
- GET /users/{id} : Récupère les informations d'un utilisateur en fonction de son "id"
Aucune donnée envoyé avec la requête
Retourne un objet avec les propriétés : firstName, lastName, email
*/

server.get("/users/:id", (request, response) => {
    // const users = [
    //     {firstName: "Sam", lastName: "Bernard", email: "sam.bernard@gmail.com"},
    //     {firstName: "John", lastName: "Swow", email: "john.snow@gmail.com"}
    // ];
    //
    // const user = users[request.params.id];

    User.findByPk(request.params.id)
        .then( async (user) => {

            if (!!user) { // n'est pas égal à null
                // Créer un nouveau post à chaque requête
                const post = await Post.create({
                    title: "Title",
                    text: "Text"
                });

                // L'associé à l'utilisateur
                await user.addPost(post);
                const posts = await user.getPosts();

                response.send({user, posts});
            } else {
                response.statusCode = 404;
                response.send({message: "No users have been found"});
            }
        })
        .catch((err) => {
            response.statusCode = 404;
            response.send("An error has occured");
        });
    //
    // if (user == null) {
    //     response.status(404).send({message: "User not found!", userId: request.params.id});
    // }
    //
    // response.send(user);
});


/*
- PUT /users/{id} : Mets à jour l'utilisateur en fonction de son id avec les données que je t'envoie
Envoyez dans la requête une donnée au format JSON : firstName, lastName
Retourne un objet contenant les informations que vous lui avez envoyé et un message disant "Utilisateur mis à jour !"
*/

server.put("/users/:id", (request, response) => {

    if (!request.body.firstName) {
        response.status(400).send("firstName missing!");
    }
    const id = request.params.id;
    // La logique pour mettre à jour l'utilisateur

    const user = {firstName: "Fiona", LastName: "Pat"}; // On récupère l'utilisateur en fonction de son id dans une base de donnée

    // Vérification que l'utilisateur a été trouvé
    if (user == null) {
        response.status(404).send({message: "User not found!", userId: request.params.id});
    }

    // Met à jour l'utilisateur récupéré
    user.firstName = request.body.firstName;
    user.lastName = request.body.firstName;
    user.email = request.body.email;

    response.send({message: "User has been updated!", id, user});
});

/*
- DELETE /users/{id} : Supprime l'utilisateur en fonction de son id
Aucune donnée envoyé avec la requête
Retourne un message "Utilisateur supprimé"
 */

server.delete("/users/:id", (request, response) => {

    const id = request.params.id;
    const user = null;

    // user.delete();

    response.send({message: "User has been deleted."});
});

server.listen(8080, "127.0.0.1", function () {
    console.log("Server is listening at http://localhost:" + PORT)
});

