const mongoose = require('mongoose');

// Créer un schéma

// Document
const CompanySchema = new mongoose.Schema({
    name: String,
    email: {
        type: String,
        default: 'ok@gmail.com',
        required: true
    },
    description: String,
    date: Date,
});

// Créer un model et le rendre exportable

module.exports = mongoose.model("Company", CompanySchema);

