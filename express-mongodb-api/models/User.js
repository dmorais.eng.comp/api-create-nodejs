const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
firstname: String,
    lastname: String,
    email: {
    type: String,
        default: "user@gmail.com",
        required: true
    },
});

module.exports = mongoose.model("User", UserSchema);