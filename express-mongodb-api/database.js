const mongoose = require('mongoose');

// Créer nouvelles connexions

mongoose.connect('mongodb://127.0.0.1/mean', {useNewUrlParser: true})
    .then(() => {
        console.log("Successfully connected!");
    })
    .catch((err) => {
        console.error("error :", err);
    });


module.exports = mongoose.connection;