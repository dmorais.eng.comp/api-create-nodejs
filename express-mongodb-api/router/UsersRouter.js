const express = require('express');

const router = express.Router();
const UsersController = require('../http/Controllers/UsersController');

const prefix = '/user';

router.get('/', UsersController.index);
router.get('/create', UsersController.create);
router.post('/', UsersController.store);
router.get('/:id', UsersController.show);
router.get('/edit/:id', UsersController.edit);
router.put('/:id', UsersController.update);
router.delete('/:id', UsersController.delete);

module.exports = {prefix, router};
