// Importer express
const express = require('express');

// Créer un router
const router = express.Router();
const CompaniesController = require('../http/Controllers/CompaniesController');

const prefix = '/companies';



// CRUD Company

// L => List
router.get('/', CompaniesController.index);
// C => Create
router.get('/create', CompaniesController.create);
// Sauvegarde ou insertion en bdd
router.post('/', CompaniesController.store);
// R => Read => Show
router.get('/:id', CompaniesController.show);
// U => Update
router.get('/edit/:id', CompaniesController.edit);
// Sauvegarder ou mettre à jour la compagnie
router.put('/:id', CompaniesController.update);
// D => Delete
router.delete('/:id', CompaniesController.delete);

// Exporter le router
module.exports = {prefix, router};