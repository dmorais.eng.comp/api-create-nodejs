const Company = require('../../models/Company');

class CompaniesController {

    index(req, res) {
        Company.find((err, companies) => {
            res.send(companies);
        });

        // Détailler la recherche

// server.get('/', (req, res) => {
//     const query = Company.find();
//     query.select("id name description");
//     query.where("name").equals("WR");
//     query.limit(4);
//     query.exec((err, companies) => (
//         res.send(companies);
// });
    }

    create(req, res) {
        res.send({categories: []});
    }

    store(req, res) {
        const company = new Company(req.body);

        company.save().then(() => {
            res.send({message: "Company created successfully!"});
        }).catch(() => {
            res.status(500).send({message: "Error!"});
        });
    }

    show(req, res) {
        const id = req.params.id;
        Company.findById(id, (err, company) => {
            res.send(company);
        });
    }

    edit(req, res) {
        const id = req.params.id;

        Company.findById(id, (err, company) => {
            res.send(company);
        });
    }

    update(req, res) {
        if(!req.body.name) {
            res.status(404).send({message: "Name required!"});
        }


        const id =req.params.id;
        Company.findByIdAndUpdate(id, req.body,{new: true},(err, company) => {

            if(!!err) {
                res.status(404).send({message: "Company not found!"});
            }

            res.send({message: "Company updated successfully!", company});
        });
    }

    delete(req, res) {
        res.send({message: "Company deleted successfully!"});
    }

}

module.exports = new CompaniesController();