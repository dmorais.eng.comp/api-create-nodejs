const User = require('../../models/User');

class UsersController {
    index(req, res) {
        User.find((err, users) => {
            res.send(users);
        });
    }

    create(req, res) {
        res.send({categories: []});
    }


    store(req, res) {
        const user = new User(req.body);

        user.save().then(() => {
            res.send({message: "User created successfully!"});
        }).catch(() => {
            res.status(500).send({message: "Error!"});
        });
    }

    show(req, res) {
        const id = req.params.id;
        User.findById(id, (err, user) => {
            res.send(user);
        });
    }

    // Revoir vidéo
    edit(req, res) {
        const id = req.params.id;
        User.findById(id, (err, user) => {
            res.send(user);
        });
    }

    update(req, res) {

        if (!req.body.email) {
            res.status(404).send({message: "Email required!"});
        }

        const id = req.params.id;
        User.findByIdAndUpdate(id, req.body, {new: true}, (err, user) => {

            if (!!err) {
                res.status(404).send({message: "User not found!"});
            }

            res.send({message: "User updated successfully!", user});

        });

    }

    // Ne s'est pas supprimé sur insomnia ni bdd lol
    delete(req, res) {
        res.send({message: "User deleted successfully!"});
    }

}

module.exports = new UsersController();