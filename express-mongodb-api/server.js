// Installer express, cors et mongoose
const express = require('express');
const cors = require('cors');

const CompaniesRouter = require('./router/CompaniesRouter');
const UsersRouter = require('./router/UsersRouter');

const server = express();
const PORT = 8080;

server.use(express.json());

server.use(cors({
    origin: "*",
    optionsSuccessStatus: 200,
}));

server.use(CompaniesRouter.prefix, CompaniesRouter.router);
server.use(UsersRouter.prefix, UsersRouter.router);



server.listen(PORT, '127.0.0.1', function () {
    console.log('Server running on http://localhost:' + PORT);
});

module.exports = server;